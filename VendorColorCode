import datetime
import glob
import os

import pandas as pd


class Generator(object):
    """
    Users' Guide:

                Arguments:
                          1.folder_path : folder contained MARA, LFA1, EINA & WTY2M
                Returns:
                      1. 0

                Workflow:
                        Buyers may request for a vendor color list to review irregularly.

                        1. MARA - get the fields: MATNR, COLOR, MAKTX
                        o material type = Z001
                        o material group = B01* and B03* (for Janice's case)
                        o X-plant status = 02 and 03
                        o color <> ' '
                        *trim material to generic level, i.e. 8 digits, paste them to table EINA

                        2. EINA - get the fields: LIFNR, MATNR, IDNLF
                        *Paste Vendor code to table LFA1

                        3. LFA1 - get the fields: LIFNR, NAME1, NAME2

                        4. WYT2M
                        o input the generic material numbers (retrieved from MARA - the first 8 digits of the material number and remove the duplicated)

                        5. Vlookup
                        *By material(8-digit) vlookup vendor, vendor name & supplier material number to MARA;
                        *Concatenate material (8-digit) and color in both MARA & WYT2M, by the combination vlookup vendor color code to MARA


    """

    def __init__(self,
                 folder_path=None):
        self.mara = pd.read_excel(glob.glob(os.path.join(folder_path, "*mara*"))[0], encoding="utf-8")
        self.lfa1 = pd.read_excel(glob.glob(os.path.join(folder_path, "*lfa1*"))[0], encoding="utf-8")
        self.eina = pd.read_excel(glob.glob(os.path.join(folder_path, "*eina*"))[0], encoding="utf-8")
        self.wyt2m = pd.read_excel(glob.glob(os.path.join(folder_path, "*wyt2m*"))[0], encoding="utf-8")

    def merge(self):
        self.mara["Material"] = self.mara["Material"].map(lambda x: str(x))
        self.eina["Material"] = self.eina["Material"].map(lambda x: str(x))
        self.mara["Material_2"] = self.mara['Material'].map(lambda x: x[0:8])
        self.eina["Vendor"] = self.eina["Vendor"].astype("str")
        self.mara = pd.merge(self.mara,
                             self.eina[["Material", "Vendor"]],
                             left_on="Material_2",
                             right_on="Material",
                             how="left")
        self.mara = pd.merge(self.mara,
                             (pd.merge(self.eina,
                                       self.lfa1,
                                       on="Vendor",
                                       how='left')[["Material", "Vendor", "Name 1"]]),
                             left_on="Material_2",
                             right_on="Material",
                             how="left")

        self.mara["Supplier Material Number"] = pd.merge(self.mara["Material_2"],
                                                         self.eina,
                                                         left_on="Material_2",
                                                         right_on="Material",
                                                         how="left")["Supplier Material Number"]
        self.mara["Ref."] = self.mara["Material_2"] + self.mara["Color"]
        self.wyt2m["Material"] = self.wyt2m["Material"].map(lambda x: str(x))
        self.wyt2m["Ref."] = self.wyt2m["Material"] + self.wyt2m["Characteristic Value"]
        self.mara = pd.merge(self.mara,
                             self.wyt2m,
                             on="Ref.",
                             how="left")[['Material_x',
                                          'Color',
                                          'Material description',
                                          'Material_2',
                                          'Vendor_y', 'Name 1', 'Ref.',
                                          'Vendor char. value']]
        return self

    def generate(self):
        self.merge()

        def get_desktop():
            desktop_path = os.path.join(os.path.expanduser("~"), "Desktop")
            return desktop_path

        file_path = os.path.join(get_desktop(), "Vendor Color Code")
        file_path2 = os.path.join(file_path, str(datetime.date.today().strftime("%Y%m%d")) + " Vendor Color Code.xlsx")
        if not os.path.exists(file_path):
            os.mkdir(file_path)
            self.mara.to_excel(file_path2,
                               encoding='utf-8',
                               index=False)
        else:
            self.mara.to_excel(file_path2,
                               encoding='utf-8',
                               index=False)
        print(file_path, " Done!")


if __name__ == "__main__":
    Generator(folder_path=r"C:\Users\chen.dong\Desktop\Vendor").generate()
