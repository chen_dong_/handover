import os

import pandas as pd


class EsgReporter(object):
    """
    Users' Guide
                Preparation:
                    1.Download Monthly Report from BO (Monthly Sales Performance Report By Items)
                    2.put reports in a folder

                Arguments:
                    1.folder: folder path of the data files

                Returns:
                    None

                Output:
                    1.Excel Format file by ItemType4

                Workflow:
                    1.Read file and its sheets
                    2.Find Month and Year
                    3.Trim DataFrames
                    4.Rename Columns
                    5.Pivot Data
                    6.Write into Excel file


    """

    def __init__(self, folder=None, mode="write"):
        self.own_ls = {}
        self.cgn_ls = {}
        self.csn_ls = {}
        self.folder = folder
        self.mode = mode

    @staticmethod
    def number_converter(a):
        if a == "-":
            return 0
        elif a == "#":
            return 0
        else:
            return int(a)

    def generate(self):

        for file in os.listdir(self.folder):
            # Own
            df = pd.read_excel(os.path.join(self.folder, file),
                               encoding='utf-8',
                               sheet_name="Own Sales Performance By Item")
            own_date = df.iloc[2, 1][5:]
            df2 = df.drop(index=[0, 1, 2, 3, 4], columns=df.columns[0], inplace=False)
            colnames = list(df2.iloc[0, :])
            df2.rename(columns=dict(zip(df2.columns, colnames)), inplace=True)
            df2.drop(index=5, inplace=True)
            for col in ['Net Receiving Qty',
                        'Net Receiving Amount', 'TY Net Sales',
                        'TY GP', "TY Quantity Sold", 'QOH (Total)', 'Closing Inventory (Total) ']:
                df2[col] = df2[col].map(lambda element: EsgReporter.number_converter(element))
            self.own_ls[own_date] = df2[df2["Store-Desc"] == "Company"].pivot_table(index=['Department-Code',
                                                                                           'Department-Desc',
                                                                                           'Vendor-Code',
                                                                                           'Vendor-Desc', "Seasonal"],
                                                                                    values=['Net Receiving Qty',
                                                                                            'Net Receiving Amount',
                                                                                            'TY Net Sales',
                                                                                            'TY GP', "TY Quantity Sold",
                                                                                            'QOH (Total)',
                                                                                            'Closing Inventory (Total) '],
                                                                                    aggfunc="sum").reset_index()[
                ['Department-Code',
                 'Department-Desc',
                 'Vendor-Code',
                 'Vendor-Desc',
                 "Seasonal",
                 'Net Receiving Qty',
                 'Net Receiving Amount',
                 'TY Net Sales',
                 'TY GP',
                 "TY Quantity Sold",
                 'QOH (Total)',
                 'Closing Inventory (Total) ']].assign(
                Month=own_date)

            # Cgn
            df_cgn = pd.read_excel(os.path.join(self.folder, file),
                                   encoding='utf-8',
                                   sheet_name="Cgn Sales Performance By Counte")
            cgn_date = df_cgn.iloc[2, 1][5:]
            df2_cgn = df_cgn.drop(index=[0, 1, 2, 3, 4], columns=df_cgn.columns[0], inplace=False)
            colnames_cgn = list(df2_cgn.iloc[0, :])
            df2_cgn.rename(columns=dict(zip(df2_cgn.columns, colnames_cgn)), inplace=True)
            df2_cgn.drop(index=5, inplace=True)
            for col in ['TY Net Sales',
                        'LY Net Sales']:
                df2_cgn[col] = df2_cgn[col].map(lambda element: EsgReporter.number_converter(element))

            self.cgn_ls[cgn_date] = df2_cgn.pivot_table(index=['Department-Code',
                                                               'Department-Desc',
                                                               'Vendor-Code',
                                                               'Vendor-Desc',
                                                               "Counter-Code",
                                                               "Counter-Desc"],
                                                        values=['TY Net Sales',
                                                                'LY Net Sales'],
                                                        aggfunc="sum").reset_index()[['Department-Code',
                                                                                      'Department-Desc', 'Vendor-Code',
                                                                                      'Vendor-Desc', "Counter-Code",
                                                                                      "Counter-Desc",
                                                                                      'TY Net Sales',
                                                                                      'LY Net Sales']].assign(
                Month=cgn_date)

            # Csn
            df_csn = pd.read_excel(os.path.join(self.folder, file),
                                   encoding='utf-8',
                                   sheet_name="Csn Sales Performance By Counte")
            csn_date = df_csn.iloc[2, 1][5:]
            df2_csn = df_csn.drop(index=[0, 1, 2, 3, 4], columns=df_csn.columns[0], inplace=False)
            colnames_csn = list(df2_csn.iloc[0, :])
            df2_csn.rename(columns=dict(zip(df2_csn.columns, colnames_csn)), inplace=True)
            df2_csn.drop(index=5, inplace=True)
            for col in ['TY Net Sales',
                        'LY Net Sales']:
                df2_csn[col] = df2_csn[col].map(lambda element: EsgReporter.number_converter(element))

            self.csn_ls[csn_date] = df2_csn.pivot_table(index=['Department-Code',
                                                               'Department-Desc',
                                                               'Vendor-Code',
                                                               'Vendor-Desc',
                                                               "Counter-Code",
                                                               "Counter-Desc"],
                                                        values=['TY Net Sales',
                                                                'LY Net Sales'],
                                                        aggfunc="sum").reset_index()[['Department-Code',
                                                                                      'Department-Desc', 'Vendor-Code',
                                                                                      'Vendor-Desc', "Counter-Code",
                                                                                      "Counter-Desc",
                                                                                      'TY Net Sales',
                                                                                      'LY Net Sales']].assign(
                Month=csn_date)

        return self

    def save(self):
        save_path = os.path.join(self.folder, "Reports")
        try:
            if not os.path.exists(os.path.join(self.folder, "Reports")):
                os.mkdir(os.path.join(self.folder, "Reports"))
            elif os.path.exists(os.path.join(self.folder, "Reports")):
                pass
        except Exception as e:
            print(e)

        if self.mode == "write":
            indicator = "w"
        elif self.mode == "append":
            indicator = "a"
        else:
            raise AttributeError("Please check MODE...!")

            # Own
        with pd.ExcelWriter(os.path.join(save_path, "Own.xlsx"),
                            mode=indicator,
                            engine='openpyxl') as writer:
            for own_key, own_value in self.own_ls.items():
                own_value.to_excel(writer, sheet_name=str(own_key), index=False, encoding="utf-8")

            # Cgn
        with pd.ExcelWriter(os.path.join(save_path, "Cgn.xlsx"),
                            mode=indicator,
                            engine='openpyxl') as writer:
            for cgn_key, cgn_value in self.cgn_ls.items():
                cgn_value.to_excel(writer, sheet_name=str(cgn_key), index=False, encoding="utf-8")

            # Csn
        with pd.ExcelWriter(os.path.join(save_path, "Csn.xlsx"),
                            mode=indicator,
                            engine='openpyxl') as writer:
            for csn_key, csn_value in self.csn_ls.items():
                csn_value.to_excel(writer, sheet_name=str(csn_key), index=False, encoding="utf-8")


if __name__ == "__main__":
    EsgReporter(folder=r"C:\Users\chen.dong\Desktop\esg", mode="write").generate().save()
